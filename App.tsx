import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  DrawerLayoutAndroid,
} from "react-native";
import React, { useState, useRef } from "react";

export default function App() {
  const drawerRef = useRef(
    null
  ) as unknown as React.MutableRefObject<DrawerLayoutAndroid>;
  let drawerOpened = false;

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={"green"} />
      <View style={styles.header}>
        <TouchableHighlight
          underlayColor="darkgreen"
          style={styles.menuButton}
          onPress={() => {
            drawerOpened
              ? drawerRef.current.closeDrawer()
              : drawerRef.current.openDrawer();
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: 35,
              width: 35,
            }}
          >
            <Image
              style={styles.menuButtonIcon}
              source={require("./outline_menu_white_24dp.png")}
            />
          </View>
        </TouchableHighlight>
        <Text style={styles.mainName}>MoneyConvertor </Text>
      </View>
      <DrawerLayoutAndroid
        onDrawerOpen={() => {
          drawerOpened = true;
        }}
        onDrawerClose={() => {
          drawerOpened = false;
        }}
        style={styles.drawer}
        ref={drawerRef}
        drawerWidth={300}
        drawerPosition="left"
        renderNavigationView={() => {
          return (
            <View
              style={{
                alignItems: "center",
                height: "100%",
                padding: 15,
              }}
            >
              <TouchableOpacity style={styles.drawerItem}>
                <Image style={styles.drawerItemImage} source={require("./outline_home_black_48dp.png")} />
                <Text style={styles.drawerItemText}>Головна</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.drawerItem}>
                <Image style={styles.drawerItemImage} source={require("./outline_settings_black_48dp.png")} />
                <Text style={styles.drawerItemText}>Налаштування</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.drawerItem}>
                <Image style={styles.drawerItemImage} source={require("./outline_money_black_48dp.png")} />
                <Text style={styles.drawerItemText}>Підтримати</Text>
              </TouchableOpacity>
            </View>
          );
        }}
      >
        <View style={styles.main}>
          <View style={styles.input}>
            <Text style={styles.inputField}>0</Text>
            <Text style={styles.inputField}>0</Text>
          </View>
          <View style={styles.keyboard}>
            <View style={styles.keyboardRow}>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>7</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>8</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>9</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>C</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.keyboardRow}>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>4</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>5</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>6</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Image
                  style={styles.backspace}
                  source={require("./outline_backspace_black_48dp.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.keyboardRow}>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>1</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>2</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>3</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Image
                  style={styles.backspace}
                  source={require("./outline_swap_vert_black_48dp.png")}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.keyboardRow}>
              <TouchableOpacity
                style={{
                  ...styles.keyboardButtonTouchable,
                  ...styles.keyboardButtonTouchableZero,
                }}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Text style={styles.keyboardButtonText}>,</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.keyboardButtonTouchable}
                onPress={() => {}}
              >
                <Image
                  style={styles.backspace}
                  source={require("./outline_refresh_black_48dp.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </DrawerLayoutAndroid>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  mainName: {
    marginLeft: 10,
    color: "white",
    fontSize: 23,
  },
  header: {
    paddingLeft: 10,
    flex: 1,
    width: "100%",
    backgroundColor: "green",
    alignItems: "center",
    flexDirection: "row",
    elevation: 30,
  },
  menuButton: {
    borderRadius: 10,
    width: 35,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
  },
  menuButtonIcon: {
    width: 30,
    height: 30,
  },
  main: {
    width: "100%",
    height: "100%",
  },
  input: {
    width: "100%",
    flex: 3,
    flexDirection: "column",
  },
  inputField: {
    width: "100%",
    fontSize: 50,
    flex: 1,
    borderBottomColor: "lightgrey",
    borderBottomWidth: 0.5,
    textAlignVertical: "center",
    textAlign: "right",
    paddingRight: 15,
  },
  keyboard: {
    flex: 7,
    backgroundColor: "lightgrey",
  },
  keyboardRow: {
    flex: 1,
    flexDirection: "row",
    //borderColor: "black",
    //borderWidth: 1,
  },
  keyboardButtonSpecial: {

  },
  keyboardButtonTouchable: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
    backgroundColor: "white",
    borderRadius: 10,
  },
  keyboardButtonTouchableZero: {
    flex: 2.25,
  },
  keyboardButtonText: {
    fontSize: 40,
    fontWeight: "bold",
  },
  backspace: {
    width: "50%",
    height: "30%",
  },
  drawer: {
    flex: 15,
    width: "100%",
  },
  drawerItem: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  drawerItemImage: {
    width: 35,
    height: 35,
    marginRight: 10,
  },
  drawerItemText: {
    fontSize: 30,
  },
});
